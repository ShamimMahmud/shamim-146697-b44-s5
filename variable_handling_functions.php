<?php

$myvar="354.28.Hello World!";

//floatval() will return the float portion(float value must be leading in the string) of myvar

$result=floatval($myvar);

echo $result."<br>";


$myvar="Hello 354.28 World!";

//floatval() will return 0 because floating value is not in the leading position, it is in the middle.

$result=floatval($myvar);

echo $result."<br>";


$myvar="Hello World!";

//floatval() will return 0

$result=floatval($myvar);

echo $result."<br>";


// I will test with myvar="", myvar=0, myvar=-0.5......
$myvar="";

$val=is_array($myvar);

if(empty($myvar)){


    echo "$myvar is empty";

}
else{

    echo "$myvar is not empty";
}

$myvar=array("Moyna","Jarina","Jahan","Hasan");

$myserializedstring=serialize($myvar);

echo $myserializedstring;

$myArray=unserialize($myserializedstring);

echo "<pre>";

print_r($myArray);

echo "<br>";
// var_dump to see the details of array
var_dump($myArray);

echo "<br>";
//This array format is usable for any php script(We can use the output in any php code...)
var_export($myArray);

//unset($myArray);



if(isset($myArray)){

    echo "This variable is set.";
}
else{

    echo "This variable is not set.";
}

echo "<pre>";


echo "<br>";



$result=boolval("asfsjfkasg");
var_dump($result);

$result=boolval("0");
var_dump($result);

$result=boolval(0);
var_dump($result);

$result=boolval(0.0000);
var_dump($result);

$result=boolval(-0.000);
var_dump($result);






?>