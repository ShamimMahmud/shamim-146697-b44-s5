<?php

//boolean example
$decision=True;
//if decision=True execute echo
if($decision)
    echo "The decision is true";
    echo "<br>";

$decision=False;
//if decision=False does not execute echo
if($decision) {
    echo "The decision is false";

}


//Integer example started
$value=100;
echo "$value";
echo "<br>";

//Floating number example started
$value=35.86;
echo "$value"."<br>";


//String Example
$var=100;
$string1='This is a single quoted string $var <br>';
$string2="This is a double quoted string $var <br>";
//This will print the exact line which is written in string1
echo "$string1";
//This will return the line with the value of var
echo "$string2";

$heredocString=<<<BITM

this is a heredoc example line1 $var <br>
this is a heredoc example line2 $var <br>
this is a heredoc example line3 $var <br>
this is a heredoc example line4 $var <br>
this is a heredoc example line5 $var <br>

BITM;

$nowdocString=<<<'BITM'

this is a nowdoc example line1 $var <br>
this is a nowdoc example line2 $var <br>
this is a nowdoc example line3 $var <br>
this is a nowdoc example line4 $var <br>
this is a nowdoc example line5 $var <br>

BITM;


echo $heredocString."<br> <br>".$nowdocString;


//Array Data Type
// Indexed Array
$arr=array(1,2,3);
print_r($arr);
echo "<br>";

$cars=array("BMW", "Toyota", "Volvo", "Ferari");
print_r($cars);
echo "<br>";
$age=array("Shamim"=>25,"Shakil"=>24, "Hasan"=>30, "Asad"=>29);
print_r($age);
echo"<br>";
echo "The age of Shamim is "."$age[Shamim]";

?>



