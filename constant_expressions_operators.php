<?php

  define('AboutSun','Sun rises in the East.');

  echo AboutSun."<br>";

  define('BITM','BITM-Basis Institute of Technology & Management');

  echo BITM;

  // _LINE_ is used for showing line numbers
  echo __LINE__."<br>";

  //Filepath
  echo __FILE__."<br>";

  //Show the function name where i am in at present
  function doSomething(){

      echo __FUNCTION__;

  }
  doSomething();

  //We can determine whether a number is odd or not by defining a funtion named isOdd() or whatever

  //=== represents the values of two numbers are equal and so also the data type(equal)
?>